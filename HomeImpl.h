#ifndef __HOMEIMPL__
#define __HOMEIMPL__

#include "Home.h"
#include "ProgressiveLed.h"
#include "Door.h"
#include "TempSensor.h"
#include "Pir.h"
#include "ProximitySensor.h"
/*Implementazione di Home.h, ci dà l'astrazione della casa*/
class HomeImpl : public Home {
private:
  ProgressiveLed * lvalue;
  Door * door;
  TempSensor * temp;
  Pir * presence;
  ProximitySensor * proxy;
  int lightvalue;
  int threshold;
public:
  HomeImpl(ProgressiveLed * lvalue, Door * door, TempSensor * temp, Pir * presence, ProximitySensor * proxy, int threshold ) {
    this->lvalue = lvalue;
    this->door = door;
    this->temp = temp;
    this->presence = presence;
    this->proxy = proxy;
    this->lightvalue = 0;
    this->lvalue->setIntensity(lightvalue);
    this->threshold = threshold;
    this->presence->init();
  }
  Door* getDoor();
  double getTemp();
  void setValue(int value);
  int getValue();
  bool isEnter();
  bool rightDistance();
};
#endif
