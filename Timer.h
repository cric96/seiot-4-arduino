#ifndef __TIMER__
#define __TIMER__
#include "Arduino.h"

//the class used to describe a timer
class Timer {
public:  
  Timer();
  void setupFreq(short freq);  
  /* period in ms */
  void setupPeriod(short period);  
  void waitForNextTick();

};


#endif

