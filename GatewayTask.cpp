#include "GatewayTask.h"
#include "MsgServiceSerial.h"
/*Controlla la correttezza del messaggio e ne crea uno nuovo da spedire sempre via usb*/
void GatewayTask::tick() {
    if(usb.isMsgAvailable(*(this->pattern))){
      Msg * msg = usb.receiveMsg();
      String temp = String(this->home->getTemp());
      String value = String(this->home->getValue());
      temp = String("T;" + temp);
      value = String("V;" + value);
      
      usb.sendMsg(value);
      usb.sendMsg(temp);
      //serve per liberare la memoria
      delete msg;
    }
}

