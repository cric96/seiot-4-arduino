#include "PirImpl.h"
#define CAL_TIME 5
PirImpl::PirImpl(int pin) {
  pinMode(pin,INPUT);
  this->pin = pin;
  this->called = false;
}

void PirImpl::init() {
  if(!called) {
    /*for(int i = 0; i < CAL_TIME; i++){
      delay(1000);
    }*/
    called = true;
  }
}

boolean PirImpl::detected() {
  if(!called) return false;
  return  digitalRead(this->pin);
}

