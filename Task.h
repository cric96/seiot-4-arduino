#ifndef __TASK__
#define __TASK__
//l'interfaccia base di un task
class Task {
private :
  int myPeriod;
  short timeElapsed;
public:
  Task(int period) {
    this->myPeriod = period;
    this->timeElapsed = 0;
  }

  virtual void tick() = 0;

  bool updateAndCheckTime(short basePeriod){
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod){
      timeElapsed = 0;
      return true;
    } else {
      return false;
    }
  }

  int getPeriod() {
    return this->myPeriod;
  }

};

#endif
