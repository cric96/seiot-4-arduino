#ifndef __SERVICEBLUETOOTH__
#define __SERVICEBLUETOOTH__
#include "MsgService.h"
#include "SoftwareSerial.h"
class MsgServiceBluetooth : MsgServiceClass {
private:
  SoftwareSerial* channel;
public:
  MsgServiceBluetooth(int rxPin, int txPin);  
  Msg* currentMsg;
  bool msgAvailable;
  void init();  
  bool isMsgAvailable();
  Msg* receiveMsg();
  //Metodo non implementato
  bool isMsgAvailable(Pattern& pattern){return false;}
  Msg* receiveMsg(Pattern& pattern);
  void sendMsg(const String& msg);  
};
extern MsgServiceBluetooth bluetooth;
#endif
