#ifndef __ProgressiveLed__
#define __ProgressiveLed__
#include "Led.h"
#include "Arduino.h"
//Un led con una luce progressiva
class ProgressiveLed : public Led{
public:
  //imposta la luminosità di un sensore e restituisce false se non è possibile settarla
  virtual boolean setIntensity(short intensity) = 0; 
};

#endif

