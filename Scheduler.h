#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "Timer.h"
#include "Task.h"
#include "Arduino.h"
#define MAX_TASKS 10
//a competitive schedulur of task
class Scheduler {
private:
  int basePeriod;
  int nTasks;
  Task* taskList[MAX_TASKS];  
  Timer timer;
  static Scheduler* SINGLETON;
  boolean initialize;
  void sleep();
  int computePeriod();
public:
  static Scheduler* instance();
  //when you call init scheduler compute the base period, you can call init once. after call init you cannot add new task.
  Scheduler();
  void init();  
  //add task in the scheduler queue
  virtual bool addTask(Task* task);  
  virtual void schedule();
  int getBasePeriod(){
    return this->basePeriod;
  }
};

#endif


