#ifndef __GATEWAYTASK__
#define __GATEWAYTASK__
#include "Task.h"
#include "MsgService.h"
#include "Home.h"
/*Pattern utilizzato per comprendere il tipo di messaggio e scegliere cosa farne*/
class GatewayPattern : public Pattern {
   boolean match(const Msg& m){
    return m.getContent() == "R";
   }
};
/*Task per gestire le richieste http da parte del gateway*/
class GatewayTask : public Task {
private:
  //astrazione della casa per conoscere temp e lvalue
  Home * home;  
  GatewayPattern * pattern;
public:
  GatewayTask(Home * home, int period) : Task(period) {
    this->home = home;
    pattern = new GatewayPattern();
  }

  void tick();
};


#endif
