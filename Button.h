#ifndef __BUTTON__
#define __BUTTON__
//un interfaccia generica per un pulsante
class Button {
 
public: 
  virtual bool isPressed() = 0;
};

#endif
