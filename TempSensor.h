#ifndef __TEMP_SENSOR__
#define __TEMP_SENSOR__

class TempSensor {
  
public:
	TempSensor(int pin);
	int readTemperature();
private:
	int pin;
	const float VOLTAGE = 5;
	const float PRECISION = 1024;
	const float MVOLTOFFSET = 500;
	const int SCALE = 1000;
  const int DIVISOR = 10;
};

#endif

