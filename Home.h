#ifndef __HOME__
#define __HOME__

#include "Door.h"
/*
astrazione generica di una casa
*/
class Home {
public:
	/* restituisce la porta installata nella casa*/
	virtual Door* getDoor() = 0;
	/* restituisce la temperatura interna della casa*/
	virtual double getTemp() = 0;
	/* setta lvalue a value*/
	virtual void setValue(int value) = 0;
	/* restituisce lvalue */
	virtual int getValue() = 0;
	/* restituisce true se qualcuno e' entrato, false altrimenti*/
	virtual bool isEnter() = 0;
	/* restituisce true se la persona e' abbastanza vicina, false altrimenti */
	virtual bool rightDistance() = 0;
};
#endif 
