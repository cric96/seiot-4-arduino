#include "Scheduler.h"
#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/power.h>

#define MAX_DIVISOR 20
Scheduler * Scheduler::SINGLETON = 0;

Scheduler * Scheduler::instance() {
  if(!Scheduler::SINGLETON) {
    Scheduler::SINGLETON = new Scheduler();
  }
  return Scheduler::SINGLETON;
}

Scheduler::Scheduler(){
  this->initialize = false;
  nTasks = 0;
  basePeriod = 0;
}
void Scheduler::init(){
  if(!this->initialize) {
    this->basePeriod = this->computePeriod();
    timer.setupPeriod(basePeriod);
    this->initialize = true;
  }
}

bool Scheduler::addTask(Task* task){
  
  if(!this->initialize){
    if (nTasks < MAX_TASKS-1){
      taskList[nTasks] = task;
      nTasks++;
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}


void Scheduler::sleep(){
  delay(50); /* fix needed to make it work */

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();

  /* Disable all of the unused peripherals. This will reduce power
   * consumption further and, more importantly, some of these
   * peripherals may generate interrupts that will wake our Arduino from
   * sleep!
   */
  power_adc_disable();
  power_spi_disable();
  //power_timer0_disable();
  //power_timer2_disable(); PWM doesn't work
  power_twi_disable();
  /* Now enter sleep mode. */
  sleep_mode();
  /* The program will continue from here after the timer timeout*/
  sleep_disable(); /* First thing to do is disable sleep. */
  /* Re-enable the peripherals. */
  power_all_enable();
}


void Scheduler::schedule(){
  if(initialize) {
    timer.waitForNextTick();
    for (int i = 0; i < nTasks; i++){
      if (taskList[i]->updateAndCheckTime(this->getBasePeriod())){
        taskList[i]->tick();
      }
    }
    sleep();
  }
}
int getMin(Task ** list, int n);
boolean isDivisor(Task ** list, int n, int val);
int Scheduler::computePeriod() {
  int min_val = getMin(taskList,nTasks);
  if(isDivisor(taskList,nTasks,min_val)){
    return min_val;
  } else {
    for(int i = min_val / 2; i > 0 ; i --) {
      if(isDivisor(taskList,nTasks,i)) {
        return i;
      }
    }
  }
  return 0;
}


int getMin(Task ** v, int n){
    int m = v[0]->getPeriod();
    for(int i = 1; i < n; i ++) {
        if(m > v[i]->getPeriod()) {
            m = v[i]->getPeriod();
        }
    }
    return m;
}
boolean isDivisor(Task ** v, int n, int value){
  for(int i = 0; i < n; i ++) {
    if(v[i]->getPeriod()% value != 0) {
      return false;
    }
  }
  return true;
};

