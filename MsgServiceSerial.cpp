#include "Arduino.h"

#include "MsgServiceSerial.h"

String content;
MsgServiceSerial usb;
bool MsgServiceSerial::isMsgAvailable(){
  return msgAvailable;
}

Msg* MsgServiceSerial::receiveMsg(){
  if (msgAvailable){
    Msg* msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;  
  } else {
    return NULL; 
  }

}

void MsgServiceSerial::init(){
  Serial.begin(9600);
  content.reserve(256);
  //messaggio che verrà costruito mano a mano che si riceve qualcosa
  content = "";
  //messaggio finale
  currentMsg = NULL;
  msgAvailable = false;  
}

void MsgServiceSerial::sendMsg(const String& msg){
  Serial.println(msg);  
}

void serialEvent() {
  /* lettura del contenuto */
  while (Serial.available()) {
    char ch = (char) Serial.read();
    if (ch == '\n'){
      usb.currentMsg = new Msg(content);
      usb.msgAvailable = true;      
    } else {
      content += ch;      
    }
  }
}

bool MsgServiceSerial::isMsgAvailable(Pattern& pattern){
  return (msgAvailable && pattern.match(*currentMsg));
}

Msg* MsgServiceSerial::receiveMsg(Pattern& pattern){
  if (msgAvailable && pattern.match(*currentMsg)){
    Msg* msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;  
  } else {
    return NULL; 
  }
}


