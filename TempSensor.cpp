#include "TempSensor.h"
#include "Arduino.h"
TempSensor::TempSensor(int pin) {
	this->pin = pin;
}

int TempSensor::readTemperature() {
	int adc = analogRead(this->pin);
  float volt = adc / PRECISION;
  float mVolt = SCALE * volt;
  return (mVolt - MVOLTOFFSET) / DIVISOR;
}
