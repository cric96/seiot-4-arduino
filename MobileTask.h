#ifndef __MOBILETASK__
#define __MOBILETASK__
#include "Task.h"
#include "Home.h"
#include "MsgService.h"
#include "Button.h"
//Array per controllare il tipo di messaggio che arriva via Bluetooth/usb nella comunicazione con il Mobile
const char protocols[] = {'L','V','T','E','F','A'};
const int protocolNumber = 6;
//Etichetta per i messaggi che arrivano/che vengono inviati, con lo scopo di facilitare la comprensione del messaggio
enum {LOGIN,LVALUE,TEMP,EXIT,ACCESS_FAILED,ACCESS_ALLOWED};
//Controlla che il messaggio arrivato segua un pattern noto e appropriato
class MobilePattern : public Pattern {
public:
  boolean match(const Msg& m){
    char content = m.getContent()[0];
    for(int i = 0; i < protocolNumber; i++) {
      if(protocols[i] == content) return true;
    }
    return false;
  }
};
/*Task che gestisce la comunicazione tra mobile-arduino-raspberry*/
class MobileTask : public Task {
private:
  //Stati che attraversa il task
   enum { WAIT_DISTANCE, CHECK_DISTANCE, WAIT_LOGIN, VERIFY_LOGIN, WAIT_ENTRY, START_SESSION } state;
   Home * home;
   Button * exit;
   MobilePattern * pattern;
   int timer;
   int maxDelay;
   int minPresenceTime;
public:
  MobileTask(Home * home, Button * exit, int period, int maxDelayTime,int minPresenceTime) : Task(period) {
    /*Stato iniziale*/
    state = WAIT_DISTANCE;
    this->home = home;
    this->exit = exit;
  	this->pattern = new MobilePattern();
  	this->timer = 0; 
  	this->maxDelay = maxDelayTime;
  	this->minPresenceTime = minPresenceTime;
  }

  void tick();
};
#endif
