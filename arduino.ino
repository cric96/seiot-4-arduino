#include "ButtonImpl.h"
#include "Sonar.h"
#include "LedImpl.h"
#include "PirImpl.h"
#include "TempSensor.h"
#include "ServoTimer2.h"
#include "Arduino.h"
#include "ArduinoDoor.h"
#include "HomeImpl.h"
#include "MobileTask.h"
#include "GatewayTask.h"
#include "Scheduler.h"
#include "MsgServiceSerial.h"
#include "MsgServiceBluetooth.h"
#include "Logger.h"
#define EXIT_PIN 8
#define LVALUE_PIN 5
#define PIR_PIN 2
#define TEMP_SENS_PIN 5
#define SERVO_PIN 3
#define ECHO 6
#define TRIG 7
#define THR 20
#define MOBILE_PERIOD 100
#define GATEWAY_PERIOD 200

#define MAX_DELAY 10000
#define MIN_PRESENCE 2000
Button * bexit;
ProximitySensor * sonar;
ProgressiveLed * lvalue;
Pir * presence;
TempSensor * temp;
ServoTimer2* servo;
Home * home;
Door * door;
MobileTask * mobile;
GatewayTask * gw;

void setup() {
  //Inizializzazione di sensori e attuatori
  deviceInit();
  //Inizializzazione dei task (gateway e mobile)
  taskInit();
}
void deviceInit() {
  usb.init();
  bluetooth.init();
  bexit = new ButtonImpl(EXIT_PIN);
  lvalue = new LedImpl(LVALUE_PIN);
  lvalue->switchOn();
  presence = new PirImpl(PIR_PIN);
  temp = new TempSensor(TEMP_SENS_PIN);
  servo = new ServoTimer2();
  sonar = new Sonar(ECHO,TRIG);
  servo->attach(SERVO_PIN);
  presence->init();
  door = new ArduinoDoor(servo);
  home = new HomeImpl(lvalue,door,temp,presence,sonar,THR);
  Logger::instance()->disable();
}
void taskInit() {
  gw = new GatewayTask(home,GATEWAY_PERIOD);
  mobile = new MobileTask(home,bexit,MOBILE_PERIOD,MAX_DELAY,MIN_PRESENCE);
  Scheduler::instance()->addTask(gw);
  Scheduler::instance()->addTask(mobile);
  Scheduler::instance()->init();
}
//Test preventivo per sensori e attuatori
void testDevice() {
  Serial.println("TEST DEVICES......");
  String isClick = bexit->isPressed() ? "click" : "no click";
  Serial.println(isClick);
  String pir = presence->detected() ? "detected" : "no detected";
  Serial.println(pir);
  Serial.print("temp = ");
  Serial.println(temp->readTemperature());
  Serial.println("motor 0 ");
  servo->write(0);
  delay(1000);
  Serial.println("motor 180");
  servo->write(2250);
  delay(1000);
  Serial.println(servo->read());
  Serial.print("distance..");
  Serial.println(sonar->getDistance());
}

//Test astrazione della porta della casa
void testHome() {
  Door * door = home->getDoor();
  Serial.println("open the door...");
  door->open();
  delay(1000);
  Serial.println("close the door..");
  door->close();
  Serial.print("temp: ");
  Serial.println(home->getTemp());
  Serial.print("entrato? :");
  Serial.println(home->getTemp());
  Serial.println("value a 50");
  home->setValue(50);
  Serial.print("value = ");
  Serial.println(home->getValue());
  delay(1000);
  Serial.println("value a 0");
  home->setValue(0);
  Serial.print("value = ");
  Serial.println(home->getValue());
  delay(1000);
  Serial.print("is enter..");
  Serial.println(home->isEnter());
  Serial.print("right distacen..");
  Serial.println(home->rightDistance());
}
void loop() {
  Scheduler::instance()->schedule();
}

