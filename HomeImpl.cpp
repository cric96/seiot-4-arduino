#include "HomeImpl.h"
//Implementazione dei metodi di HomeImpl.h
Door* HomeImpl::getDoor(){
  return this->door;
}

double HomeImpl::getTemp(){
  return this->temp->readTemperature();
}

void HomeImpl::setValue(int value){
  this->lightvalue = value;
  this->lvalue->setIntensity(lightvalue);
}

int HomeImpl::getValue(){
  return this->lightvalue;
}

bool HomeImpl::isEnter(){
  this->presence->detected();
}

bool HomeImpl::rightDistance(){
  return this->threshold > this->proxy->getDistance();
}

