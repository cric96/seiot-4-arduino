#include "MobileTask.h"
#include "MsgServiceSerial.h"
#include "MsgServiceBluetooth.h"
#include "Logger.h"
//Tempo per definire un limite entro cui effettuare il login
#define MAX_LOGIN_TIME 10000
//Funzione utilizzata per splittare una stringa: serve per ottenere il contenuto del messaggio
String splitString(String str, char sep, int index){
 int found = 0;
 int strIdx[] = { 0, -1 };
 int maxIdx = str.length() - 1;
 
 for (int i = 0; i <= maxIdx && found <= index; i++)
 {
    if (str.charAt(i) == sep || i == maxIdx)
    {
      found++;
      strIdx[0] = strIdx[1] + 1;
      strIdx[1] = (i == maxIdx) ? i+1 : i;
    }
 }
 return found > index ? str.substring(strIdx[0], strIdx[1]) : "";
}

void MobileTask::tick() {
  Msg * rcv = NULL;
  switch(state) {
    //Si aspetta che l'utente entri nel range prestabilito per fare il login
    case WAIT_DISTANCE:
      Logger::instance()->log("wait..");
      if(home->rightDistance()) {
		  state = CHECK_DISTANCE;
		this->timer = 0;
      }
    break;
    //Si controlla che l'utente permanga nel range prestabilito prima di fargli fare il login
	case CHECK_DISTANCE:
    Logger::instance()->log("check distance..");
		this->timer += this->getPeriod();
		if (!home->rightDistance()) {
			state = WAIT_DISTANCE;
			bluetooth.sendMsg("O"); //se non si permane alla distanza corretta per minPresenceTime secondi
		}
		else if(this->timer > this->minPresenceTime) {
			state = WAIT_LOGIN;
			bluetooth.sendMsg("L"); //possibilita di loggarsi nel sistema
			this->timer = 0;
		}
	break;
  //Si attende che venga effettuato il login entro un timeout (per evitare deadlock)
    case WAIT_LOGIN:
    Logger::instance()->log("wait login..");
    //messo per problemi di dead lock
    this->timer += this->getPeriod();
    if(this->timer > MAX_LOGIN_TIME) {
      this->timer = 0;
      this->state = WAIT_DISTANCE;
      bluetooth.sendMsg("O");
    }
    if((rcv = bluetooth.receiveMsg(*(this->pattern)))!= NULL) {
      String msg = rcv->getContent();
		  if (msg[0] == protocols[LOGIN]) {
			  state = VERIFY_LOGIN;
        //invio dati al gateway per la verifica delle credenziali
			  usb.sendMsg(msg);
        this->timer = 0;
		  }
    }
    break;

    case VERIFY_LOGIN:  
    Logger::instance()->log("verify login..");
		if (usb.isMsgAvailable()) {
      rcv = usb.receiveMsg(*(this->pattern));
			String msg = rcv->getContent();
      if(msg[0] ==  protocols[ACCESS_FAILED]) {
          bluetooth.sendMsg("N"); //accesso negato, credenziali sbagliate
          state = WAIT_LOGIN;
      } else if(msg[0] ==  protocols[ACCESS_ALLOWED]) {
          state = WAIT_ENTRY;
          this->timer = 0;
          bluetooth.sendMsg("A"); //accesso permesso, credenziali corrette
          this->home->getDoor()->open();
      }
		}
    break;
    //Attendo che la persona entri nella casa entro Maxdelay secondi, altrimenti chiudo la sessione
    case WAIT_ENTRY:
    Logger::instance()->log("wait entry..");
		this->timer += this->getPeriod();
		if (this->timer > this->maxDelay) {
			state = WAIT_DISTANCE;
      this->home->getDoor()->close();
			usb.sendMsg("E"); //sessione terminata se non si entra nella casa entro max delay
			bluetooth.sendMsg("E");
		}
		if (this->home->isEnter()) {
			state = START_SESSION;
			usb.sendMsg("S"); //inizio della sessione
			bluetooth.sendMsg("S");
		}  
    break;

    case START_SESSION:    
    Logger::instance()->log("start session..");
    if (this->exit->isPressed()) {
      this->timer = 0;
      state = WAIT_DISTANCE;
      this->home->getDoor()->close();
      usb.sendMsg("F"); //se premo il pulsante exit su door
      bluetooth.sendMsg("F");
    }
    if(bluetooth.isMsgAvailable()) {
      rcv = bluetooth.receiveMsg(*(this->pattern));
      String msg = rcv->getContent();
      if (msg[0] == protocols[EXIT]) {
        state = WAIT_DISTANCE;
        this->home->getDoor()->close();
        usb.sendMsg("F"); //se premo exit da mobile
      }
      else if (msg[0] == protocols[LVALUE]) {
        String value = splitString(msg,';',1);
        this->home->setValue(value.toInt());
      }
      else if (msg[0] == protocols[TEMP]) {
        String value = String((int)this->home->getTemp());
        String temp = String("T;" + value);
        bluetooth.sendMsg(temp);
      }
    }
    break;
  }
  if(rcv != NULL) {
    delete rcv;
  }
}


