#include "ArduinoDoor.h"
//Valori da passare a ServoTimer2 per muovere il motorino, simulando apertura e chiusura
#define CLOSE 0
#define OPEN 2250

void ArduinoDoor::open() {
	if (!this->isopen) {
		this->servo->write(OPEN);
    this->isopen = true;
	}
}

void ArduinoDoor::close() {
	if (this->isopen) {
		this->servo->write(CLOSE);
    this->isopen = false;
	}
}

bool ArduinoDoor::isOpen() {
	return this->isopen;
}
