#ifndef __DOOR__
#define __DOOR__
/*
astrazione di una porta generica
*/
class Door {
public:
	/*apre la porta*/
	virtual void open() = 0;
	/*chiude la porta*/
	virtual void close() = 0;
	/*dice se la porta e' aperta o chiusa*/
	virtual bool isOpen() = 0;
};
#endif
