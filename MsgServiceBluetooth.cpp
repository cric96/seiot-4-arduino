#include "Arduino.h"
#include "MsgServiceBluetooth.h"
#define RX 10
#define TX 11

MsgServiceBluetooth bluetooth(TX,RX);
String bluetoothContent;

MsgServiceBluetooth::MsgServiceBluetooth(int rxPin, int txPin){
  channel = new SoftwareSerial(rxPin, txPin);
}

void MsgServiceBluetooth::init(){
  bluetoothContent.reserve(256);
  channel->begin(9600);
  
}

void MsgServiceBluetooth::sendMsg(const String& msg){
  channel->println(msg);  
}

bool MsgServiceBluetooth::isMsgAvailable(){
  return channel->available();
}

Msg* MsgServiceBluetooth::receiveMsg(){
  if (channel->available()){  
    bluetoothContent="";
    while (channel->available()) {
      bluetoothContent += (char)channel->read();      
    }
    return new Msg(bluetoothContent);
  } else {
    return NULL;  
  }
}
Msg* MsgServiceBluetooth::receiveMsg(Pattern& pattern){
  Msg * received = this->receiveMsg();
  if(received == NULL) {
    return NULL;
  } else if(pattern.match(received->getContent())) {
    return received;
  } else {
    return NULL;
  }
}
