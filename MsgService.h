#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"
/**
 * classe radice di tutti i messaggi
 */
class Msg {
  String content;

public:
  Msg(String content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};
/**
 * Strategia per controllare i messaggi
 */
class Pattern {
public:
  virtual boolean match(const Msg& m) = 0;  
};
/*Gestore dei messaggi che vengono ricevuti/inviati*/
class MsgServiceClass {
    
public: 
  
  Msg* currentMsg;
  bool msgAvailable;
  //inizializza il gestore
  virtual void init() = 0;  

  virtual bool isMsgAvailable() = 0;
  //legge il messaggio ricevuto e lo "consuma"
  virtual Msg* receiveMsg() = 0;

  virtual bool isMsgAvailable(Pattern& pattern) = 0;
  //Legge il messaggio "consumandolo" solo se segue il pattern
  virtual Msg* receiveMsg(Pattern& pattern) = 0;
  virtual void sendMsg(const String& msg) = 0;
};

#endif

