#ifndef __SERVICESERIAL__
#define __SERVICESERIAL__
#include "MsgService.h"
class MsgServiceSerial : MsgServiceClass {
public:
  Msg* currentMsg;
  bool msgAvailable;

  void init();  

  bool isMsgAvailable();
  Msg* receiveMsg();

  bool isMsgAvailable(Pattern& pattern);
  Msg* receiveMsg(Pattern& pattern);
  
  void sendMsg(const String& msg);  
};
extern MsgServiceSerial usb;
#endif
