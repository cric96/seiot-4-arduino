#ifndef __ARDUINO_DOOR__
#define __ARDUINO_DOOR__
#include "Door.h"
#include "ServoTimer2.h"

class ArduinoDoor : public Door {
private:
	ServoTimer2 * servo;
	bool isopen;
public:
	ArduinoDoor(ServoTimer2 * servo) {
		this->servo = servo;
		this->isopen = false;
	}
	void open();

	void close();

	bool isOpen();
};
#endif
